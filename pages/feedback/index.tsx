import HeadContent from "../../components/HeadContent";
import {
    Button,
    Col,
    Container,
    FormGroup,
    Input,
    Row,
    Form,
} from "reactstrap";
import Footer from "../../components/Footer";
import Header from "../../components/Header";
import styles from "../../styles/Feedback.module.css";
import {Formik, Field, FieldProps} from 'formik';
import {useState} from "react";
import Error from "../../components/Error";
import Loading from "../../components/Loading";

interface FormValues {
    email: string;
    feedbackMessage: string;
}

interface StatusValues {
    status: number;
    info: string;
}

const EmailInput = ({ field, form:{ touched, errors }, ...props }: FieldProps) =>
    <Input {...field} {...props}
           id={styles["form-input"]}
           className={`${styles["form-input-field"]} ${touched.feedback && errors.feedback ? styles["form-input-field-error"] : ''}`}
           placeholder="Your email"
           type="email"
           rows={10} />;

const FeedbackMessageInput = ({ field, form:{ touched, errors }, ...props }: FieldProps) =>
    <Input {...field} {...props}
           id={styles["form-input"]}
           className={`${styles["form-input-field"]} ${touched.feedback && errors.feedback ? styles["form-input-field-error"] : ''}`}
           placeholder="Your feedback"
           type="textarea"
           rows={10}/>;

const Feedback = () => {
    const [sendStatus, setSendStatus] = useState<StatusValues>();

    const validateFeedbackMessage = (value:FormValues) => {
        let error
        if (!value) {
            error = 'Required';
        }
        return error;
    }

    const validateEmail = (value: FormValues["email"]) => {
        let error;

        const regexp = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/;
        if (value !== '' && !value.match(regexp)) {
            error = 'Please enter a valid email address';
        }
        return error;
    }

    const postFeedback = async (values:FormValues) => {
        const res = await fetch('/api/sendFeedback', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(values),
        })

        if (!res.ok) {
            const { cause } = await res.json();
            setSendStatus({status: res.status, info: cause});
        } else {
            setSendStatus({status: res.status, info: 'ok'});
        }
    }

    return (
        <>
            <HeadContent/>
            <Header title={"Feedback"} />
            <main className="main-content">
                <Container>
                    <Row className="justify-content-center">
                        <Col xs="9">
                            <h5 className={styles["target-title"]}>You can leave your comments about ClassCrawler here.</h5>
                            <Formik
                                initialValues={{
                                    email: '',
                                    feedbackMessage: '',
                                }}
                                onSubmit={async (
                                    values,
                                    {setSubmitting, resetForm}
                                ) => {
                                    await postFeedback(values);
                                    setSubmitting(false);
                                    resetForm({
                                        values: {
                                            email: '',
                                            feedbackMessage: '',
                                        },
                                    });
                                }}
                            >
                                {({
                                      handleSubmit,
                                      handleBlur,
                                      handleChange,
                                      values,
                                      touched,
                                      errors,
                                      isSubmitting,
                                      isValidating}) =>
                                    sendStatus !== undefined && sendStatus.info !== "ok" ?
                                        <Error statusCode={sendStatus.status} title={sendStatus.info}/> :
                                        isSubmitting && !isValidating ? <Loading message="Sending"/> :
                                        <Form onSubmit={handleSubmit}>
                                            <FormGroup>
                                                <Field component={EmailInput} onChange={handleChange}
                                                       onBlur={handleBlur}
                                                       value={values.email}
                                                       validate={validateEmail}
                                                       name="email"
                                                />
                                                {touched.email && errors.email && <div className={styles["error-hint"]}>{errors.email}</div>}
                                            </FormGroup>
                                            <FormGroup>
                                                <Field component={FeedbackMessageInput} onChange={handleChange}
                                                       onBlur={handleBlur}
                                                       value={values.feedbackMessage}
                                                       validate={validateFeedbackMessage}
                                                       name="feedbackMessage"
                                                />
                                                {touched.feedbackMessage && errors.feedbackMessage && <div className={styles["error-hint"]}>{errors.feedbackMessage}</div>}
                                            </FormGroup>
                                            <Button
                                                type="submit"
                                                disabled={isSubmitting || !values.feedbackMessage || Boolean(errors.email)}
                                                className={styles["button-submit"]}>
                                                Send
                                            </Button>
                                        </Form>
                                }
                            </Formik>
                        </Col>
                    </Row>
                </Container>
            </main>
            <Footer/>
        </>
    )
}

export default Feedback
