import type { AppProps } from 'next/app'
import 'bootstrap/dist/css/bootstrap.css';
import '../styles/styles.css'

function ClassCrawler({ Component, pageProps }: AppProps) {

  return <Component {...pageProps} />
}

export default ClassCrawler
