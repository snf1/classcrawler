import styles from '../styles/404.module.css'
import {Alert} from "reactstrap";

const NotFound = () => (
    <div className={styles["danger-wrapper"]}>
        <Alert className={styles.alert}>
            <h4 className="alert-heading">
                404
            </h4>
            <hr />
            <p className="mb-0">
                This page could not be found. Please go back to the&nbsp;
                <a className="alert-link" href="/">
                    ClassCrawler page
                </a>
                .
            </p>
        </Alert>
    </div>
)

export default NotFound
