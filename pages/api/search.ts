import {NextApiRequest, NextApiResponse} from "next";
import { connectToDatabase } from '../../utils/mongodb';

export default async function handler(req: NextApiRequest, res:NextApiResponse) {
    switch (req.method) {
        case 'GET': {
            try {
                const query = req.query

                const q = query.q;
                const cursor = Number(query.cursor);
                const sizePerReq = 20;
                if (typeof q !== "string") {
                    throw new Error("Query param 'q' has to be of type string")
                }
                let mongoDBQuery = JSON.parse(q);
                const {db} = await connectToDatabase();
                const methodsCollection = db.collection('methods');
                // @ts-ignore
                const data = await methodsCollection.find(mongoDBQuery)
                    .skip(cursor > 0 ? ( ( cursor - 1 ) * sizePerReq ) : 0 )
                    .limit( sizePerReq )
                    .toArray();

                const historyCollection = db.collection('history');
                await historyCollection.insertOne({
                    mongoDbQuery: q,
                    date: new Date(),
                    foundMethods: data.length,
                    statusOfReq: true
                })

                return res.status(200).json(data);
            } catch (e) {
                const {db} = await connectToDatabase()
                const historyCollection = db.collection('history');
                await historyCollection.insertOne({
                    mongoDbQuery: req.query.q,
                    date: new Date(),
                    foundMethods: 0,
                    statusOfReq: false
                })

                if (e instanceof Error) return res.status(400).json({cause: e.message})
                return res.status(400).json({cause: String(e)})
            }
        }
    }
}
