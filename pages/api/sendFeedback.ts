import {NextApiRequest, NextApiResponse} from "next";

export default async function handler(req: NextApiRequest, res:NextApiResponse) {
    switch (req.method) {
        case 'POST': {
            try {
                const email = req.body['email'];
                const feedbackMessage = req.body['feedbackMessage'];

                const url = process.env.WEBHOOK_URL || '';
                const messageRes = await fetch(
                    url,
                    {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            text: `:speech_balloon: *New Feedback*\n\n*User*: ${!email ? 'anonymous' : email}\n*Message*: ${feedbackMessage}`,
                        }),
                    }
                )

                if (!messageRes.ok) {
                    throw new Error(`${await messageRes.text()}`)
                }

                return res.status(200).json({status: 'ok'});
            } catch (e) {
                if (e instanceof Error) return res.status(400).json({cause: e.message})
                return res.status(400).json({cause: String(e)})
            }
        }
    }
}
