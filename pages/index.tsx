import type { NextPage} from 'next'
import {Container, Row, Col} from 'reactstrap';
import styles from '../styles/MainPage.module.css'
import SearchSection from "../components/SearchSection";
import HeadContent from "../components/HeadContent";
import Result from "../components/Result";
import Loading from "../components/Loading";
import useSWRInfinite from "swr/infinite";
import {useRouter} from "next/router";
import {useEffect, useState} from "react";
import {FetchError} from "../types/FetchError";
import Footer from "../components/Footer";
import Error from "../components/Error";
import Header from "../components/Header";
import {Method} from "../types/Method";
import InfiniteScroll from "react-infinite-scroll-component";

const SearchPage: NextPage|null = () => {
   const useHasMounted = () => {
        const [hasMounted, setHasMounted] = useState(false);
        useEffect(() => {
            setHasMounted(true);
        }, []);
        return hasMounted;
    }

    const useMongoReq = (query:string|string[]|undefined) => {
        const getKey = (pageIndex: number, previousPageData: Array<Method>) => {
            pageIndex = pageIndex + 1;
            if (!query || previousPageData && !previousPageData.length) return null
            return `/api/search?q=${query}&cursor=${pageIndex}`;
        }
        const fetcher = async (url: RequestInfo) => {
            const res = await fetch(url)
            if (!res.ok) {
                const JSONres = await res.json()
                throw new FetchError(
                    JSONres.cause, res.status, 'An error occurred while fetching the data.'
                )
            }

            return res.json()
        }

        const { data, error, size, setSize } = useSWRInfinite(getKey, fetcher, {
            revalidateFirstPage: false
        })
        const isLoadingInitialData = !error && !data;
        const isEmpty = data?.[0]?.length === 0;
        const sizePerReq = 20;
        const isReachingEnd =
            isEmpty || (data && data[data.length - 1]?.length < sizePerReq);

        return {
            mongoDBResp: data?.flat(),
            isLoadingInitialData,
            isError: error,
            isReachingEnd,
            size,
            setSize
        }
    }

    const router = useRouter();
    let mongoDBQuery = router.query.q

    const hasMounted = useHasMounted();
    const { mongoDBResp, isLoadingInitialData, isError, isReachingEnd, size, setSize } = useMongoReq(mongoDBQuery)

    if (!hasMounted) {
        return null;
    }

    return (
        <>
            <HeadContent/>
            <Header title={"ClassCrawler"}/>
            <main className="main-content">
                <Container>
                    <Row className="justify-content-center">
                        <Col xs="9">
                            <SearchSection loading={Boolean(mongoDBQuery) && isLoadingInitialData} inputQuery={mongoDBQuery}/>
                            <div className={styles["repo-wrapper"]}>
                                <span className={styles["icon-repo"]}/>
                                <span className={styles["repo-name"]}>{'mediawiki / core'}</span>
                            </div>
                            {!mongoDBQuery ? null :
                                isError ?  <Error statusCode={isError.status} title={isError.info}/> :
                                <InfiniteScroll
                                    next={() => setSize(size + 1)}
                                    hasMore={!isReachingEnd}
                                    loader={<Loading message="Searching"/>}
                                    dataLength={mongoDBResp?.length ?? 0}
                                >
                                    <Result parsedCode={mongoDBResp}/>
                                </InfiniteScroll>}
                        </Col>
                    </Row>
                </Container>
            </main>
            <Footer/>
        </>
    )
}

export default SearchPage
