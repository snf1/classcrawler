import {connectToDatabase} from "../../utils/mongodb";
import HeadContent from "../../components/HeadContent";
import styles from "../../styles/HistoryPage.module.css";
import {Col, Container, Row, Table} from "reactstrap";
import {HistoryEntry} from "../../types/HistoryEntry";
import Footer from "../../components/Footer";
import Header from "../../components/Header";

const HistoryPage = ({historyEntries}: {historyEntries: Array<HistoryEntry>}) => (
    <>
        <HeadContent/>
        <Header title={"History of MongoDB queries"}/>
        <main className="main-content">
            <Container>
                <Row className="justify-content-center">
                    <Col xs="9">
                        <Table>
                            <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Query
                                </th>
                                <th>
                                    Number of found methods
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {historyEntries.map((entry, key) => (
                                <tr key={key}>
                                    <th scope="row">
                                        {key + 1}
                                    </th>
                                    <td>
                                        <a
                                            className={styles["query"]}
                                            href={"/?q=" + encodeURIComponent(entry._id)}
                                        >
                                            {entry._id}
                                        </a>
                                    </td>
                                    <td>
                                        {entry.foundMethods}
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>
        </main>
        <Footer/>
    </>
)

export async function getServerSideProps() {
    const {db} = await connectToDatabase()
    const historyCollection = db.collection('history');
    const lastSuccessQueries = await historyCollection.aggregate([
        { $match: {"statusOfReq": true}},
        {$group: {_id: "$mongoDbQuery", foundMethods: {$last: "$foundMethods"} , date: {$last: "$date"}}},
        {$sort: {date: -1}},
        { $limit : 20 }
    ]).toArray()

    return {
        props: {
            historyEntries: JSON.parse(JSON.stringify(lastSuccessQueries))
        },
    }
}

export default HistoryPage
