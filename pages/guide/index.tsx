import HeadContent from "../../components/HeadContent";
import {Col, Container, Row} from "reactstrap";
import ReactMarkdown from 'react-markdown';
import Footer from "../../components/Footer";
import {Prism as SyntaxHighlighter} from 'react-syntax-highlighter'
import Header from "../../components/Header";
import Error from "../../components/Error";
import {GetServerSideProps} from "next";

interface ServerSideStatus {
    code: number
    message: string
}

const Guide = ({text, status}: {text: string, status: ServerSideStatus}) => (
    <>
        <HeadContent/>
        <Header title={"ClassCrawler guide"} />
        <main className="main-content">
            <Container>
                <Row className="justify-content-center">
                    <Col xs="9">
                        {status.code === 500 ?
                            <Error statusCode={status.code} title={status.message}/> :
                            <ReactMarkdown
                                children={text}
                                components={{
                                    code({node, inline, className, children, ...props}) {
                                        const match = /language-(\w+)/.exec(className || '')
                                        return !inline && match ? (
                                            <SyntaxHighlighter
                                                children={String(children).replace(/\n$/, '')}
                                                language={match[1]}
                                                PreTag="div"
                                                {...props}
                                            />
                                        ) : (
                                            <code className={className} {...props}>
                                                {children}
                                            </code>
                                        )
                                    }
                                }}
                            />
                        }
                    </Col>
                </Row>
            </Container>
        </main>
        <Footer/>
    </>
)

export const getServerSideProps:GetServerSideProps = async ({res}) => {
    let text:string = ''
    let message:string

    try {
        let res = await fetch('https://gitlab.com/snf1/classcrawler/-/raw/master/GUIDE.md')
        text = await res.text()
        message = 'OK'
    } catch (e) {
        res.statusCode = 500
        message = "An error occurred!"
    }

    return {
        props: {
            text: text,
            status: { code: res.statusCode, message: message }
        },
    }
}

export default Guide
