import {Db, MongoClient} from 'mongodb'

let cachedClient: MongoClient | null = null;
let cachedDb: Db | null = null;

export async function connectToDatabase() {
  // check the cached.
  if (cachedClient && cachedDb) {
    // load from cache
    return {
      client: cachedClient,
      db: cachedDb,
    }
  }

  const opts = {}

  const mongodbUri = process.env.NEXT_PUBLIC_MONGODB_URI
  const mongodb_db = process.env.NEXT_PUBLIC_MONGODB_DB
  // Connect to cluster
  let client = new MongoClient(mongodbUri ?? '', opts)
  await client.connect()
  let db = client.db(mongodb_db)

  // set cache
  cachedClient = client
  cachedDb = db

  return {
    client: cachedClient,
    db: cachedDb,
  }
}
