This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

Installation of Package Manager:
```bash
npm install --global yarn
```

Setup environments:
```bash
yarn install
```

Run the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Usage

After opening [http://localhost:3000](http://localhost:3000) you will be able to use the search field based on the MediaWiki codebase.

The search format is performed in the style of a query to MongoDB.

At the moment we can only search by **methods**. An interface for searching by **classes** and **variables** will be available in the nearest future.

_**Examples of query request:**_
- We could find the methods where IContextSource pass as argument and this method didn't have any overrides.
```json
{"arguments.types": {"$in": ["IContextSource"]},"overrides": []}
```
- We could find the place where method IContextSource::getStats is overridden. In this case we could replace usage of this deprecated method with a new one.
```json
{"name": "getStats", "returnTypes": {"$in": ["IBufferingStatsdDataFactory"]}}
```
- We could find the place where we still return Title type. There are no overrides/overridden and it wasn't used in factory for Title creation. In this case all places that we found could be replaced with interfaces if needed.
```json
{"returnTypes": {"$in": ["Title"]}, "overrides": [], "overridden": [], "fullName": {"$not": { "$regex": "Factory"}}}
```

## Collection "methods" structure

```json
{
    "name": "string",
    "code": "string",
    "fullName": "string",
    "overrides": [
        {
            "index": "string"
        }
    ],
    "overridden": [
        {
            "index": "string"
        }
    ],
    "returnTypes": [
        {
            "index": "string"
        }
    ],
    "tags": [
        {
            "name": "string",
            "description": "string"
        }
    ],
    "arguments": [
        {
            "index": {
                "name": "string",
                "types": [
                    {
                        "index": "string"
                    }
                ]
            }  
        }
    ]
}
```

_**Fields details:**_

- **name** (name of the method).

_Example:_ 
```json
{
    "name": "getUser"
}
```

- **code** (display of the method in its original form with implementation).

_Example:_
```php
public function setReason( $msg ) {
    $this->configuredReadOnly->setReason( $msg ); 
}
```

- **fullName** (complete information about the location of the method).

_Example:_
```json
{
    "fullName": "MediaWiki\Linker\LinkRenderer::castToTitle, MediaWiki\Storage\PageUpdater::getTitle"
}
```

- **overrides** (an array of places from where current method overrides).

_Example:_
```json
{
    "0": "IContextSource::getStats"
    "1": "ContextSource::getStats"
}
```

- **overridden** (an array of places where the current method was overridden).

_Example:_
```json
{
    "0": "ContextSource::getStats"
    "1": "DerivativeContext::getStats"
    "2": "RequestContext::getStats"
}
```

- **returnTypes** (an array of types that returned from current method).

_Example:_
```json
{
    "0": "bool"
    "1": "void"
}
```

- **tags** (an array of tags that used for current method).

_Example:_
```json
{
    "0": {
        "name": "since",
        "description": "1.32"
    }
}
```

- **arguments**  (an array of arguments that are in the method signature).
    - name (name of the argument).
    - types (an array of types of the argument).

_Example:_
```json
{
    "0": {
        "name": "user",
        "types": [
            {
                "0": "User"
            }
        ]
    }
}
```

## Learn More

To learn more about MongoDB query take a look at the following resource:
- [MongoDB Query Documentation](https://docs.mongodb.com/manual/reference/operator/query/) - learn about MongoDB query details.

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
