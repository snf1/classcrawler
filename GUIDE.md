# GUIDE

ClassCrawler is an extremely fast and structured code search engine. It parses php 7.4+ code into  MongoDB collections what allows to use MongoDB's query language for searching through methods and classes. It’s fast and easily adaptable – you can find what you need within seconds.

## Usecases

[List of all methods that return `IBufferingStatsdDataFactory`](https://classcrawler.prettyclear.com/?q=%7B%22returnTypes%22%3A+%7B%22%24in%22%3A+%5B%22IBufferingStatsdDataFactory%22%5D%7D%7D)


```json
{"returnTypes": {"$in": ["IBufferingStatsdDataFactory"]}}
```

[List of methods where `IContextSource` is passed as an argument](https://classcrawler.prettyclear.com/?q=%7B%22arguments.types%22%3A+%7B%22%24in%22%3A+%5B%22IContextSource%22%5D%7D%7D)

```json
{"arguments.types": {"$in": ["IContextSource"]}}
```

[List of methods that overrides `MediaWiki\Preferences\PreferencesFactory::getForm`](https://classcrawler.prettyclear.com/?q=%7B%22overrides%22%3A+%7B%22%24in%22%3A+%5B%22MediaWiki%5C%5CPreferences%5C%5CPreferencesFactory%3A%3AgetForm%22%5D%7D%7D)

```json
{"overrides": {"$in": ["MediaWiki\\Preferences\\PreferencesFactory::getForm"]}}
```

[List of methods where `IContextSource` is passed as an argument and method should overrides another one](https://classcrawler.prettyclear.com/?q=%7B%22arguments.types%22%3A+%7B%22%24in%22%3A+%5B%22IContextSource%22%5D%7D%2C%22overrides%22%3A+%7B+%22%24regex%22%3A+%22%5BA-Za-z%5D*%22%7D%7D)
```json
{"arguments.types": {"$in": ["IContextSource"]},"overrides": { "$regex": "[A-Za-z]*"}}
```

## List of top-level fields:

- `name: String` Name of method, i.e. `getForm`
- `fullName: String` Name of method with class and namespace, i.e. `MediaWiki\Preferences\PreferencesFactory::getForm`
- `overrides: String\[\]` List of fullNames of methods that are overridden by this one (methods in parent classes or interfaces)
- `overridden: String\[\]` List of fullNames of methods that override this one (in descedant classes)
- `returnTypes: String\[\]` List of types returned by given method, i.e. `[string, null]`.
- `arguments: { name: String, types: String[]}\[\]` List of arguments acceped by method, i.e. `[{ name: "context", types: [IContextSource] }]`.
- `since/see/stable/deprecated: String` Tags of the method. `{ since: "1.38" }`
- `hardDeprecated: { status: Boolean, version: String }` Object of hard deprecation status and version by given method. `{ status: "true", version: "1.38"}`

## Useful features

- Copy URL to share your results. Example: `https://classcrawler.prettyclear.com/?q=%7B%22since%22%3A%20%20%221.35%22%7D`
- Browse search history to know what others are looking for.
- Interact with your result to get more information. Example: [Select](https://imgur.com/a/Dks2FK3) any method from your results to automatically generate query request.
