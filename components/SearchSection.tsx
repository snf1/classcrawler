import styles from "../styles/SearchSection.module.css";
import {Button, Input, InputGroup, Form} from "reactstrap";
import {useRouter} from "next/router";

const SearchSection = ({loading, inputQuery}:{
    loading: boolean,
    inputQuery: string|string[]|undefined
}) => {
    const router = useRouter()

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        let formData =  new FormData(event.currentTarget);
        let request = formData.get("request") as string;
        if (!request) return;

        router.push({
            pathname: '/',
            query: { q: request },
        })
    }

    return (
        <Form onSubmit={handleSubmit}>
            <InputGroup className={styles["input-wrapper"]}>
                <Input className={styles["input"]} name={"request"} defaultValue={inputQuery ?? undefined}/>
                <Button className={styles["search-button"]} type={"submit"} disabled={loading}/>
            </InputGroup>
        </Form>
    )
}

export default SearchSection
