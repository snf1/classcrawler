import styles from "../styles/Header.module.css";
import {Col, Container, Nav, NavItem, NavLink, Row} from "reactstrap";
import {useRouter} from "next/router";

interface LinkContent {
    link: string
    linkPrev: string
}

const Header = ({title}: {title: string}) => {
    const navLinks: Array<LinkContent> = [
        {"link": "/", "linkPrev": "Search page"},
        {"link": "/history", "linkPrev": "History"},
        {"link": "/guide", "linkPrev": "Guide"},
        {"link": "/feedback", "linkPrev": "Share your feedback"}
    ]
    const router = useRouter();

    return (
        <header className={styles["header"]}>
            <Container>
                <Row className="justify-content-center">
                    <Col xs="9">
                        <h4 className={styles["main-title"]}>{title}</h4>
                        <nav>
                            <Nav className={"d-flex justify-content-center"}>
                                {navLinks.map((contentObj, key) =>
                                    <NavItem key={key} className={styles["item"]}>
                                        <NavLink
                                            className={`${styles['link']} ${router.pathname === contentObj.link ? styles['active'] : ""}`}
                                            href={contentObj.link}
                                        >
                                            {contentObj.linkPrev}
                                        </NavLink>
                                    </NavItem>)}
                            </Nav>
                        </nav>
                    </Col>
                </Row>
            </Container>
        </header>
    )
}

export default Header
