import styles from "../styles/MethodDocsSection.module.css";

const MethodDocsSection = ({docsData, intro}: {docsData: Array<string>, intro: string}) => {

    if (!docsData || !docsData.length) return null
    return (
        <div className={styles["docs_section"]}>
            <div className={styles["docs_section-intro"]}>{intro}</div>
            {
                docsData.map((method, key) => {
                    const escapedMethod = method.replace(/\\/g, '\\\\')
                    return (
                        <div className={styles["docs_section-content"]} key={key}>
                            <a
                                href={"/?q=" + encodeURIComponent(`{"fullName": "${escapedMethod}"}`)}
                            >
                                {method}
                            </a>
                        </div>
                    )
                })
            }
        </div>
    )
}

export default MethodDocsSection
