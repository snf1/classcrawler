import styles from "../styles/Loading.module.css";
import busyIcon from "../public/busy.gif";

const Loading = ({message}: {message: string}) => (
    <div className={styles["loading-wrapper"]}>
        <img src={busyIcon.src} alt="busy icon"/>
        <div>{message}...</div>
    </div>
)

export default Loading
