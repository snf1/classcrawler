import styles from '../styles/Error.module.css'
import {Alert} from "reactstrap";

const Error = ({statusCode, title}:{
    statusCode: number,
    title: string
}) => (
    <div className={styles["error-wrapper"]}>
        <Alert className={styles.alert}>
            <h4 className="alert-heading">{statusCode}</h4>
            <hr />
            <p className="mb-0">{title}</p>
        </Alert>
    </div>
)

export default Error
