import {Method} from "../types/Method";
import styles from "../styles/Result.module.css";
import ParsedMethod from "./ParsedMethod";

const Result = ({parsedCode}: {parsedCode: Array<Method>|undefined}) => {
    if (parsedCode && !parsedCode.length) {
        return (
            <div className={styles["no-result"]}>
                <span>“Nothing for you, Dawg.”</span>
                <div>0 results</div>
            </div>
        )
    }
    return (
        <div className="result">
            {parsedCode?.map((obj, key) => <ParsedMethod method={obj} key={key} />)}
        </div>
    )
}

export default Result
