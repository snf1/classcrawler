import styles from "../styles/ParsedMethod.module.css";
import {Method} from "../types/Method";
import MethodDocsSection from "./MethodDocsSection";

const ParsedMethod = ({method}: {method: Method}) => {
    const buildParamsString = (method: Method) => {
        let args:Array<string> = []

        method.arguments.forEach((arg) => {
            let types = arg.types.length ? `${arg.types.join('|')} ` : ''
            args.push(`${types}${arg.name}`)
        })

        return args
    }

    const availableTags = [
        "codeCoverageIgnore", "deprecated", "internal", "stable", "since", "see", "todo", "unstable", "warning"
    ]
    const foundTags = Object.keys(method).filter((element: string) => availableTags.includes(element));
    // @ts-ignore
    const nameDescrTags = foundTags.map((tag) => `@${tag} ${method[tag]}\n`);

    return(
        <div className={styles.method}>
            <div className={styles.title}>
                {method.fullName}
                ({buildParamsString(method).map((param, i, arr) =>
                    <div className={styles.param} key={i}>
                        {param}
                        {i + 1 === arr.length ? '' : ','}
                    </div>
                )})
                {method.returnTypes.length ? `: ${method.returnTypes.join('|')}` : ''}
            </div>
            <div className={styles["docs"]}>
                {
                    method.hasOwnProperty('hardDeprecated') && method.hardDeprecated.status ?
                        <div className={styles["docs_section"]}>
                            <div className={styles["docs_section-intro"]}>
                                {
                                    method.hardDeprecated.hasOwnProperty('version') ?
                                        <>Hard deprecated since {method.hardDeprecated.version}</> :
                                        <>Hard deprecated</>
                                }
                            </div>
                        </div>
                        : null
                }
                <MethodDocsSection docsData={method.overrides} intro={"The method overrides:"}/>
                <MethodDocsSection docsData={method.overridden} intro={"The method is overridden by:"}/>
                <MethodDocsSection docsData={method.callees} intro={"The callees:"}/>
                <MethodDocsSection docsData={method.callers} intro={"The callers:"}/>
                {
                    foundTags.length ?
                        <div className={styles["docs_section"]}>
                            <div className={styles["docs_section-intro"]}>The method's tags:</div>
                            <div className={styles["docs_section-content"]}>
                                {nameDescrTags}
                            </div>
                        </div>
                        : null
                }
            </div>
            <div className={styles["match-code"]}>
                {
                    method.code.split('\n').map((line, key) =>
                            <div className={styles.line} key={key}>
                                <span className={styles.lnum}>{key + 1}</span>
                                <span className={styles.lval}>{line}</span>
                            </div>
                    )
                }
            </div>
        </div>
    )
}

export default ParsedMethod
