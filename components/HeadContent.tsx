import Head from "next/head";

const HeadContent = () => (
    <Head>
        <title>ClassCrawler</title>
        <meta name="description" content="Help to research your code" />
        <link rel="icon" href="/favicon.svg" />
    </Head>
)

export default HeadContent
