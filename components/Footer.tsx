import styles from "../styles/Footer.module.css";
import {Col, Container, Row} from "reactstrap";

const Footer = () => (
    <footer>
        <Container>
            <Row className="justify-content-center">
                <Col xs="9">
                    <h6 className={styles["top-links"]}>
                        ClassCrawler search is powered by <a href="https://speedandfunction.com/">Speed and
                        Function</a>.
                        <br/>
                        <a href="https://gitlab.com/snf1/classcrawler">Source code</a> is available under the
                        terms of the GPL v3 or any later version.
                    </h6>
                </Col>
            </Row>
        </Container>
    </footer>
)

export default Footer
