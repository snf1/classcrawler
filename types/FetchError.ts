export class FetchError extends Error {
    info: string;
    status: number

    constructor(info: string, status: number, msg: string) {
        super(msg);
        Object.setPrototypeOf(this, FetchError.prototype);

        this.info = info;
        this.status = status;
    }
}
