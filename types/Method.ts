export interface Method {
    name: number
    code: string
    fullName: string
    overrides: Array<any>
    overridden: Array<any>
    callees: Array<any>
    callers: Array<any>
    returnTypes: Array<any>
    arguments: Array<any>
    hardDeprecated: {
        status: boolean
        version: string
    }
}
